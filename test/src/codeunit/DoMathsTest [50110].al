codeunit 50110 "Do Maths Test"
{
    Subtype = Test;

    [Test]
    procedure TestAdd()
    var
        DoMaths: Codeunit "Do Maths";
    begin
        assert.AreEqual(4, DoMaths.Add(2, 2), '2+2 is 4');
        assert.AreEqual(8, DoMaths.Add(4, 4), '4+4 is 8');
        assert.AreEqual(16, DoMaths.Add(8, 8), '8+8 is 16');
    end;

    [Test]
    procedure TestSub()
    var
        DoMaths: Codeunit "Do Maths";
    begin
        assert.AreEqual(4, DoMaths.sub(5, 1), '5-1 is 4');
        assert.AreEqual(8, DoMaths.Sub(10, 2), '10-2 is 8');
        assert.AreEqual(16, DoMaths.Sub(20, 4), '20-4 is 16');
    end;

    [Test]
    procedure TestFib()
    var
        DoMaths: Codeunit "Do Maths";
    begin
        assert.AreEqual(3, DoMaths.fib(4), 'should be 3');
        assert.AreEqual(34, DoMaths.fib(9), 'should be 34');
        assert.AreEqual(144, DoMaths.fib(12), 'should be 144');
    end;

    var
        assert: Codeunit Assert;
}
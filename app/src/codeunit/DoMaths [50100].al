codeunit 50100 "Do Maths"
{
    procedure Add(numA: Integer; numB: Integer): Integer
    begin
        exit(numA + numB);
    end;

    procedure Sub(numA: Integer; numB: Integer): Integer
    begin
        exit(numA - numB);
    end;

    procedure fib(n: Integer): Integer
    begin
        if n <= 1 then exit(n);
        exit(fib(n - 1) + fib(n - 2));
    end;
}